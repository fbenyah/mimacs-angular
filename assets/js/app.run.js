(function () {
    'use strict';

    var app = angular.module('app');

    app.run(['$rootScope', function ($rootScope) {
    	
    	// Global Variables
        $rootScope.currentUser = {};
        $rootScope.application = {};
        
        $rootScope.currentUser.IsAuthenticated = false;
        
        $rootScope.application.PageTitle = "Mimacs XP";
        $rootScope.application.ApplicationName = "Mimacs XP";
        
        $rootScope.baseApiUrl = 'http://base.api.endpoint/';
        $rootScope.localJsonData = '/assets/js/data/demoData.json';
    }]);
})();