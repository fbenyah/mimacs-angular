﻿(function() {
    "use strict";

    var app = angular.module("app");

    app.config(["$stateProvider", "$urlRouterProvider", stateConfigurator]);

    function stateConfigurator($stateProvider, $urlRouterProvider) {
        "use strict";
        
        $urlRouterProvider.when("/", "/home");
        $urlRouterProvider.when("", "/home");
        $urlRouterProvider.when("/main", "/home");
        $urlRouterProvider.otherwise("/home");
        
        $stateProvider
        
        .state("shell", {
            url: "",
            templateUrl: "layout/main.html",
            controller: "main",
            abstract: true
        })
        .state("home", {
        		parent: "shell",
                url: "/home",
                views : {
                	'topSection' : {
                		templateUrl: "layout/menu/adminDashboardMenu.html"
                	},
                	'leftSidebar' : {
                		templateUrl: "layout/sidebar/leftSidebar.html"
                	},
                	'rightSidebarTop' : {
                		templateUrl: "layout/sidebar/search.html"
                	},
                	'rightSidebarBottom' : {
                		templateUrl: "layout/sidebar/rightSidebar.html"
                	}
                }
        })

        .state("users", {
                parent: "shell",
                url: "/users",
                views : {
                    'topSection' : {
                        templateUrl: "layout/menu/systemAdminDashboardMenu.html"
                    },
                    'leftSidebar' : {
                        templateUrl: "layout/sidebar/leftSidebar.html"
                    },
                    'rightSidebarTop' : {
                        templateUrl: "layout/sidebar/search.html"
                    },
                    'rightSidebarBottom' : {
                        templateUrl: "layout/sidebar/rightSidebar.html"
                    },
                    'content': {
                        templateUrl: "admin/users/dashboard.html"
                    }
                }
        })
        .state("login", {
        		parent: "shell",
                url: "/login",
                views : {
                	'content' : {
                		templateUrl: "login/login.html",
                        controller: "login as vm"
                	}
                }
        });
    };

})();
