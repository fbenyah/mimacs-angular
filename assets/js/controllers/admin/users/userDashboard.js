(function () {
    "use strict";
    var controllerId = "userDashboard";
    angular.module("app").controller(controllerId, ["common", "$scope", "$rootScope", "$state", "$uibModal", "ValidationService", "NetworkService", login]);

    function login(common, $scope, $rootScope, $state, $uibModal, validationService, networkService) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);
        var logError = common.logger.getLogFn(controllerId, "error");
        var logSuccess = common.logger.getLogFn(controllerId, "success");
        
        var vm = this;

        vm.users = []

        vm.addUser = function () {
            var addUserModal = $uibModal.open({
                animation: true,
                templateUrl: '/views/layout/modals/admin/addUser.html',
                controller: 'addUser'
            });
        }

        $rootScope.$on('userAdded', function(ev, data){
            vm.users.push(data);
            console.log(vm.users);
        });

	    activate();
        setupData();
	
	    function activate() {        
	        common.activateController([], controllerId)
	            .then(function () {
	                log('Activated user dashboard View');
	            });
	
	        $scope.$emit(controllerId);
	    }

        function setupData(){
            networkService.GetCall($rootScope.localJsonData).then(function(results) {
                vm.treedata = results.data.users;
            });
        }
    };
})();
