(function () {
    "use strict";
    var controllerId = "logout";
    angular.module("app").controller(controllerId, ["common", "$scope", "$rootScope", "$state", logout]);

    function logout(common, $scope, $rootScope, $state) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);
        var logError = common.logger.getLogFn(controllerId, "error");
        var logSuccess = common.logger.getLogFn(controllerId, "success");
        
        var vm = this;

    	vm.logout = function () {
            if ($rootScope.currentUser.IsAuthenticated){
                $rootScope.currentUser.IsAuthenticated = false;
                logSuccess("You have been logged out.");
                $state.go('login');
            }
            else{
                logError("You are currently not logged in.");
            }
    	}

	    activate();
	
	    function activate() {        
	        common.activateController([], controllerId)
	            .then(function () {
	                log('Activated Logout Layout View');
	            });
	
	        $scope.$emit(controllerId);
	    }
    };
})();
