(function () {

    "use strict";
    var controllerId = "loggedInUser";
    angular.module("app").controller(controllerId, ["common", "$scope", "$rootScope", loggedInUser]);

    function loggedInUser(common, $scope, $rootScope) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);
        var logError = common.logger.getLogFn(controllerId, "error");
        var logSuccess = common.logger.getLogFn(controllerId, "success");
        
        var vm = this;

	    activate();
	
	    function activate() {        
	        common.activateController([], controllerId)
	            .then(function () {
	                log('Activated Logged In User Layout View');
	            });
	
	        $scope.$emit(controllerId);
	    }
    };
})();
