(function () {
    "use strict";
    var controllerId = "systemDetails";
    angular.module("app").controller(controllerId, ["common", "$scope", "$rootScope", systemDetails]);

    function systemDetails(common, $scope, $rootScope) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);
        var logError = common.logger.getLogFn(controllerId, "error");
        var logSuccess = common.logger.getLogFn(controllerId, "success");
        
        var vm = this;

	    activate();
	
	    function activate() {        
	        common.activateController([], controllerId)
	            .then(function () {
	                log('Activated System Details Layout View');
	            });
	
	        $scope.$emit(controllerId);
	    }
    };
})();
