(function () {
    "use strict";
    var controllerId = "logo";
    angular.module("app").controller(controllerId, ["common", "$scope", "$rootScope", logo]);

    function logo(common, $scope, $rootScope) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);
        var logError = common.logger.getLogFn(controllerId, "error");
        var logSuccess = common.logger.getLogFn(controllerId, "success");
        
        var vm = this;
        vm.logo = "SCMS";

	    activate();
	
	    function activate() {        
	        common.activateController([], controllerId)
	            .then(function () {
	                log('Activated Logo Layout View');
	            });
	
	        $scope.$emit(controllerId);
	    }
    };
})();
