(function () {
    "use strict";
    var controllerId = "main";
    angular.module("app").controller(controllerId, ["common", "$scope", "$rootScope", main]);

    function main(common, $scope, $rootScope) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);
        var logError = common.logger.getLogFn(controllerId, "error");
		var logSuccess = common.logger.getLogFn(controllerId, "success");
        
        var vm = this;


	    activate();
	
	    function activate() {        
	        common.activateController([], controllerId)
	            .then(function () {
	                log('Activated Main Layout View');
	            });
	
	        $scope.$emit(controllerId);
			logSuccess("You are on the home screen");
	    }
    };
})();
