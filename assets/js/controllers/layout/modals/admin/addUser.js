(function () {
    "use strict";
    var controllerId = "addUser";
    angular.module("app").controller(controllerId, ["common", "$scope", "$rootScope", "$uibModalInstance", addUser]);

    function addUser(common, $scope, $rootScope,  $uibModalInstance) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);
        var logError = common.logger.getLogFn(controllerId, "error");
        var logSuccess = common.logger.getLogFn(controllerId, "success");
        
        var vm = this;

        $scope.userData = {
            firstName: '',
            lastName: '',
            cell: '',
            email: '',
            address: ''
        }

        $scope.cancelAdd = function () {
            $uibModalInstance.dismiss('cancel');
        };

        $scope.addNewContact = function (isValid) {

            if(isValid){
                $rootScope.$broadcast('userAdded', $scope.userData);
                console.log($scope.userData);
                $uibModalInstance.close();
            }
        }

	    activate();
	
	    function activate() {        
	        common.activateController([], controllerId)
	            .then(function () {
	                log('Activated Add User View');
	            });
	
	        $scope.$emit(controllerId);
	    }
    };
})();
