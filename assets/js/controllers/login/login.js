(function () {
    "use strict";
    var controllerId = "login";
    angular.module("app").controller(controllerId, ["common", "$scope", "$rootScope", "$state", "ValidationService", "NetworkService", login]);

    function login(common, $scope, $rootScope, $state, validationService, networkService) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);
        var logError = common.logger.getLogFn(controllerId, "error");
        var logSuccess = common.logger.getLogFn(controllerId, "success");
        
        var vm = this;

        vm.logInUser = function (form) {
            if (!$rootScope.currentUser.IsAuthenticated){
                if(form.$valid) {
                    if (vm.userDetails.email){
                        if (vm.userDetails.password){
                            if (validationService.IsValidEmail(vm.userDetails.email)){
                                
                                var results = networkService.GetCall($rootScope.localJsonData).then(function(results) {
                                    if (results){
                                        if (results.status == 200){
                                            var response = results.data;

                                            if ((vm.userDetails.email == response.user.email) && (vm.userDetails.password == response.user.password)) {
                                                $rootScope.currentUser.IsAuthenticated = true;
                                                logSuccess("You are logged in successfully");
                                                $state.go('users');
                                            }
                                            else{
                                                logError("Email or password incorrect");
                                            }

                                        }
                                    }
                                                
                                });
                                
                            }
                        }
                    }
                }
                else{
                    logError("Invalid form!");
                }
            }
            else{
                logError("You are already logged in.");
            }
        }

	    activate();
	
	    function activate() {        
	        common.activateController([], controllerId)
	            .then(function () {
	                log('Activated Login View');
	            });
	
	        $scope.$emit(controllerId);
	    }
    };
})();
