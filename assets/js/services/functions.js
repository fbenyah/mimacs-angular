﻿(function () {
    "use strict";

    var serviceId = "FunctionsService";
    angular.module("app").factory(serviceId,
    [
        "common", "$http", "$rootScope",
        function (common, $http, $rootScope) {
            var service = {};

            var logError = common.logger.getLogFn(serviceId, "error");
            var logSuccess = common.logger.getLogFn(serviceId, "success");

            service.ReplaceAll = function (str, find, replace) {
                if (str && find) {
                    return str.replace(new RegExp(find, 'g'), replace);
                }
                return str;
            };

            service.TrimString = function (str) {
                if (str) {
                    return str.replace(/(^\s*)|(\s*$)/g, "");
                }

                return "";
            };

            service.LeftTrimString = function (str) {
                if (str) {
                    return str.replace(/(^\s*)/g, "");
                }
                return "";
            };

            service.RightTrimString = function (str) {
                if (str) {
                    return str.replace(/(\s*$)/g, "");
                }
                return "";
            };

            // return only the digits in the string
            service.GetDigits = function (str) {
                if (str) {
                    return str.replace(/[^\d]/g, "");
                }
                return "";
            };          


            // file upload
            service.UploadFile = function (file, documentType, organizationId,directorId,documentDescription) {

                if (file.size < 1024 * 1024 * 2) {

                    var fileContentType = file.type;
                    var base64File;
                    var postData;
                    var reader = new FileReader();
                    reader.onload = function (readerEvt) {


                        logSuccess("Starting File upload");
                        
                        var binaryString = readerEvt.target.result;
                        var binary = '';
                        var bytes = new Uint8Array(reader.result);
                        var len = bytes.byteLength;

                        //convert to string
                        for (var i = 0; i < len; i++) {
                            binary += String.fromCharCode(bytes[i]);
                        }

                        //convert to base64 string
                        base64File = btoa(binary);
                        
                        postData = {
                            OrganizationId: organizationId,
                            DirectorId: directorId,
                            DocumentDataString: base64File,
                            Name: file.name,
                            Type: documentDescription,
                            DocumentType: documentType
                        };

                        $http.post($rootScope.baseApiUrl + "UploadDocument/", postData)
                         .success(function (data) {

                             var dataCleaned = data;
                             dataCleaned = data.replace(/"/g, '');
                             console.log('cleaned data');

                             if (documentType === $rootScope.rainfinDocument.BeeCertificate) {
                                 $rootScope.edDocument.BeeCertificate = dataCleaned;
                             }
                             else if (documentType === $rootScope.rainfinDocument.ProofOfAddress) {
                                 if (organizationId === $rootScope.profileData.director.directorId) {
                                     $rootScope.edDocument.ProofOfAddressPersonal = dataCleaned;
                                 }
                                 else {
                                     $rootScope.edDocument.ProofOfAddressBusiness = dataCleaned;
                                 }                                 
                             }
                             else if (documentType === $rootScope.rainfinDocument.IdCopy) {
                                 $rootScope.edDocument.IdCopy = dataCleaned;
                             }
                             else if (documentType === $rootScope.rainfinDocument.CM1) {
                                 $rootScope.edDocument.CM1 = dataCleaned;
                             }
                             else if (documentType === $rootScope.rainfinDocument.CM2) {
                                 $rootScope.edDocument.CM2 = dataCleaned;
                             }
                             else if (documentType === $rootScope.rainfinDocument.BankStatement) {
                                 $rootScope.edDocument.ThreeMonthBankStatement = dataCleaned;
                             }
                             else if (documentType === $rootScope.rainfinDocument.FinancialStatement) {
                                 $rootScope.edDocument.FinancialStatement = dataCleaned;
                             }
                             else if (documentType === $rootScope.rainfinDocument.TaxClearanceCertificate) {
                                 $rootScope.edDocument.TaxClearanceCertificate = dataCleaned;
                             }
                             else if (documentType === $rootScope.rainfinDocument.CompanyLogo) {                                 
                                 $rootScope.profileData.companyLogo = dataCleaned;
                                 $rootScope.$broadcast("logoUploaded", data);
                             }

                             logSuccess("File upload successfull");                             
                         }).error(function (data, status, headers, config) {
                             logError("Unable to upload file");
                         });
                    };

                    reader.readAsArrayBuffer(file);                   
                }
                else {
                    logError("File must be less than 2MB");
                }                
            };

            service.ViewFile = function (fileId) {
                if ($rootScope.profileData.companyId) {
                    var url = "https://sme-test.rainfin.com/zaf/eng/Download/EdFile/" + fileId + "/" + $rootScope.profileData.companyId;
                    window.open(url);
                } else {
                    logError("Organization could not be identified!");
                }
            };

            return service;
        }
    ]);
})();