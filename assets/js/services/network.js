﻿(function() {
    "use strict";

    var serviceId = "NetworkService";
    angular.module("app").factory(serviceId,
    [
        "common", "$state", "$q", "$http",
        function (common, $state, $q, $http) {
            var service = {};

            var getLogFn = common.logger.getLogFn;
            var log = getLogFn(serviceId);
            var logError = common.logger.getLogFn(serviceId, "error");
            var logSuccess = common.logger.getLogFn(serviceId, "success");

            

            service.GetCall = function (url) {
                if (url) {
                    
                   return $http.get(url);

                } else {
                    logError("No url supplied.");
                }                
            };

            service.PostCall = function (url, data) {
                if (url) {
                    if (data){
                        $http.post(url, data)
                        .success(function (response) {
                            log("success", response);
                        }).error(function (data, status, headers, config) {
                            log("error", data);
                        });
                    }
                    else{
                        logError("No data supplied.");
                    }
                } else {
                    logError("No url supplied.");
                }
            };           

            return service;
        }
    ]);
})();