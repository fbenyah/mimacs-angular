(function() {
    "use strict";

    var serviceId = "AuthenticationService";
    angular.module("app").factory(serviceId,
    [
        "common", "$state", "$http", "$rootScope",
        function (common, $state, $http, $rootScope) {
            var service = {};

            var logError = common.logger.getLogFn(serviceId, "error");
            var logSuccess = common.logger.getLogFn(serviceId, "success");

            service.IsLoggedIn = function () {
                if ($rootScope.profileData.userName) {
                    if (!$rootScope.profileData.userLoggedIn) {
                        $http.post($rootScope.baseApiUrl + "IsLoggedIn/", { UserName : $rootScope.profileData.userName }).success(function (response) {
                            $rootScope.profileData.userLoggedIn = true;
                        }).error(function (data, status, headers, config) {
                            $rootScope.profileData.userLoggedIn = false;
                            logError("You are currently not logged in!");
                            $state.go("signin");
                        });
                    }                    
                } else {
                    $rootScope.profileData.userLoggedIn = false;
                    logError("Seems your session expired.");
                    $state.go("signin");
                }                
            };

            service.Login = function (username, password) {
                $http.post($rootScope.baseApiUrl + "Login/", { UserName: username, Password: password })
                    .success(function (response) {
                        $rootScope.profileData = response;
                        $rootScope.profileData.userName = username;
                        $rootScope.profileData.userLoggedIn = true;
                        $rootScope.profileData.ficaCompletion = 30;
                        $rootScope.profileData.actionCentreCompletion = 60;
                        $rootScope.profileData.bankingCompletion = 80;
                        $rootScope.profileData.financialsCompletion = 10;
                        $rootScope.profileData.peopleCompletion = 90;
                        $rootScope.profileData.demographicsCompletion = 70;
                        $rootScope.profileData.businessActivitiesCompletion = 30;
                        $rootScope.loanApplicationStatus.OverallProgress = 70;
                        $state.go("profile");
                    })
                    .error(function (data, status, headers, config) {
                        $rootScope.profileData.userLoggedIn = false;
                        logError("Unable to process login!");
                        $state.go("signin");
                    });
            };

            service.Logout = function () {
                if ($rootScope.profileData.userName) {
                    if ($rootScope.profileData.userLoggedIn) {
                        $http.post($rootScope.baseApiUrl + "LogOut/", { UserName : $rootScope.profileData.userName }).success(function (response) {
                            $rootScope.profileData.userLoggedIn = false;
                            $rootScope.$broadcast("logoutUser", $rootScope.profileData.userLoggedIn);
                            logSuccess("You have been successfully logged out!");
                            $state.go("signin");
                        }).error(function (data, status, headers, config) {
                            $rootScope.profileData.userLoggedIn = false;
                            logError("Unable to process logout!");
                        });
                    }
                    else {
                        $rootScope.profileData.userLoggedIn = false;
                        logError("You are not logged in at that moment.");
                        $state.go("signin");
                    }
                } else {
                    $rootScope.profileData.userLoggedIn = false;
                    logError("Seems you are not logged in at that moment.");
                    $state.go("signin");
                }
            };            

            return service;
        }
    ]);
})();