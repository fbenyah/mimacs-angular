﻿(function () {
    "use strict";

    var serviceId = "ValidationService";
    angular.module("app").factory(serviceId,
    [
        "common",
        function (common) {
            var service = {};

            var logError = common.logger.getLogFn(serviceId, "error");

            service.IsNonBlank = function (str) {
                if (str) {
                    return String(str).match(/\S/);
                }

                logError("Empty input received");
                return false;
            };

            service.IsWholeNumber = function (str) {
                if (str) {
                    return String(str).match(/^\s*\d+\s*$/);
                }

                logError("Empty input received");
                return false;
            };

            service.IsSignedInteger = function (str) {
                if (str) {
                    return String(str).match(/^\s*(\+|-)?\d+\s*$/);
                }

                logError("Empty input received");
                return false;
            };

            service.IsDecimal = function (str) {
                if (str) {
                    return String(str).match(/^\s*(\+|-)?((\d+(\.\d+)?)|(\.\d+))\s*$/);
                }

                logError("Empty input received");
                return false;
            };

            // similar to isDecimal only that only 0 or 2 digits are allowed after the decimal point
            service.IsAmount = function (str) {
                if (str) {
                    return String(str).match(/^\s*(\+|-)?((\d+(\.\d\d)?)|(\.\d\d))\s*$/);
                }

                logError("Empty input received");
                return false;
            };

            service.IsValidEmail = function (str) {
                if (str) {
                    return String(str).match(/^\s*[\w\-\+_]+(\.[\w\-\+_]+)*\@[\w\-\+_]+\.[\w\-\+_]+(\.[\w\-\+_]+)*\s*$/);
                }

                logError("Empty input received");
                return false;
            };

            service.IsStrictAlphaNumberic = function (str) {
                if (str) {
                    return String(str).match(/^([0-9]|[a-z])+([0-9a-z]+)$/i);
                }

                logError("Empty input received");
                return false;
            };

            service.IsValidCreditCardType = function (cardType, cardNumber) {
                if (cardType && cardNumber) {
                    var creditCardList = [
	                   //type      prefix   length
	                   ["amex",    "34",    15],
	                   ["amex",    "37",    15],
	                   ["disc",    "6011",  16],
	                   ["mc",      "51",    16],
	                   ["mc",      "52",    16],
	                   ["mc",      "53",    16],
	                   ["mc",      "54",    16],
	                   ["mc",      "55",    16],
	                   ["visa",    "4",     13],
	                   ["visa",    "4",     16]
                    ];

                    var cc = getDigits(cardNumber);

                    if (luhn(cc)) {
                        for (var i in creditCardList) {
                            if (creditCardList[i][0] == (cardType.toLowerCase())) {
                                if (cc.indexOf(creditCardList[i][1]) == 0) {
                                    if (creditCardList[i][2] == cc.length) {
                                        return true;
                                    }
                                }
                            }
                        }
                    }
                    return false;
                }

                logError("Empty input received");
                return false;
            };

            service.IsInLengthRange = function (str, minLength, maxLength) {
                if (str && minLength && maxLength) {
                    var okRegEx = new RegExp("^[a-z0-9_-]{" + minLength + "," + maxLength + "}$");
                    return okRegEx.test(String(str));
                }

                return false;
            };

            service.IsValidSaCellNumber = function (str) {
                if (str) {
                    return String(str).match(/((?:\+27|27)|0)([678][1234689])(\d{7})$/);
                }

                logError("Empty input received");
                return false;
            };

            // return only the digits in the string
            function getDigits(str) {
                if (str) {
                    return str.replace(/[^\d]/g, "");
                }
                return "";
            }

            function luhn(cc) {
                var sum = 0;
                var i;

                for (i = cc.length - 2; i >= 0; i -= 2) {
                    sum += Array(0, 2, 4, 6, 8, 1, 3, 5, 7, 9)[parseInt(cc.charAt(i), 10)];
                }

                for (i = cc.length - 1; i >= 0; i -= 2) {
                    sum += parseInt(cc.charAt(i), 10);
                }

                return (sum % 10) == 0;
            }

            return service;
        }
    ]);
})();