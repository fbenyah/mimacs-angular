(function () {
    'use strict';

    var app = angular.module('app', [
        // Angular modules
        'ngAnimate',        // animations
        'ui.router',        // routing
        'ngSanitize',       // sanitizes html bindings (ex: sidebar.js)
        'ngCookies',

        // Custom modules
        'common',           // common functions, logger, spinner
        'common.bootstrap', // bootstrap dialog wrapper functions

        // 3rd Party Modules
        'angularMoment',
        'ui.bootstrap',
        'angularTreeview'
    ]);

})();